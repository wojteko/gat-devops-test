# Kubernetes automation design

Your goal is to create automation that will make deploying the applications to a Kubernetes cluster and configuration management effortless for the development team. 

Given a simple application (ie. https://hub.docker.com/r/nginxdemos/hello), create necessary Kubernetes configuration that will allow hosting of the application in the cluster.

We should be able to take the provided code, and deploy it to a given Kubernetes cluster that is already running and configured using provided instructions.

As this role is senior level, the reasoning behind your choice of technologies and design decisions is as important as the code that you will provide. If any additional considerations should be taken into account, such as design ideas that would take too long to implement given the time constraints, please document them as well.

**Constraints/Assumptions:**
* You can assume to have running Kubernetess cluster
* Your solutions is going to be tested on clean minikube
* The application should not have any single points of failure
* End-user traffic should be encrypted
* It should be possible to easily change pod parameters and deploy multiple named “environments”, either via a templating system or other tool
* The system should be designed with the developer in mind - redeploying the application in a different version should be as easy as possible
* This assignment should take 4-6 hours to complete

**Fork this repository**
* All your changes should be made in a private fork of this repository. When you're done please, please:
* Share your fork with the @wojteko user (Settings -> Members -> Share with Member)
* Make sure that you grant user the Reporter role, so that our reviewers can check out the code using Git.